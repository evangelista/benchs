Files in this directory list instances to be used in our experiences:

* `base` contains instances to be used in both experiences (deadlock
  and liveness)
* `liveness` contains instances to be used only in liveness
  experiences