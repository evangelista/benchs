# Requirements

## Install prod

See installation procedure at http://www.tcs.hut.fi/Software/prod/

## Install sbench

requires python >= 3.9

```
git clone git@depot.lipn.univ-paris13.fr:evangelista/sbench.git
cd sbench
python3 setup.py install --user
```

## Install pompet

requires python >= 3.9

```
git clone git@depot.lipn.univ-paris13.fr:evangelista/pompet.git
cd pompet
python3 setup.py install --user
pompet compile-checker
```

## Download pnml files to be used in experiments

```
git clone git@depot.lipn.univ-paris13.fr:evangelista/mcc-models.git
```

## Download benchmark scripts

```
git clone git@depot.lipn.univ-paris13.fr:evangelista/benchs.git
cd benchs/stub
```
From now on, all commands must be launched from this directory.



# Preparation

Create a symbolic link to the model directory of the mcc-models
repository cloned before:
```
ln -s path/to/mcc-models/models in/pnml
```

Generate 5 randomized pnml files for each model instance:
```
scripts/gen-pnml
```

Compile pnml files to executable:
```
scripts/gen-exe
```

Generate sbench benchmark description files:
```
scripts/gen-sbench
```


# Execution

Launch benchmarks:
```
sbench -rv out/sbench/sbench-pompet.json
sbench -rv out/sbench/sbench-prod.json
sbench -rv out/sbench/sbench-pompet-prov.json
```


# Extraction

Extract data and check everything went fine:
```
scripts/extract
scripts/merge
scripts/check
```

Generate figures and html pages:
```
scripts/gen-figures
scripts/gen-models > out/html/models.html
cp in/html/* out/html/
```

View results:
```
firefox out/html/index.html
```